<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CartListRequest;
use App\Models\Product;



class CartController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function show(Request $request)
    {
        $carrito = $request->session()->get("carrito",[]);
        return view("cart", ["carrito"=>$carrito]);
    }

    public function addToChart(CartListRequest $request, $id)
     {
         $product = Product::where("id", $id)->first();

         $carrito = $request->session()->get("carrito",[]);
         array_push($carrito, ["id"=>$product->$id,"img_preview"=>$product->img_preview,"name"=>$product->name,"price"=>$product->price]);
         $request->session()->put("carrito",$carrito);

         return redirect()->action([CartController::class,"show"]);
     }
}
