<html>
  <head>
    <title>PHP Test</title>
  </head>
  <body>
    <?php
    class Empleado{
      private $nombre;
      private $sueldo;
      public function inicializar($nom,$sue)
      {
        $this->nombre=$nom;
        $this->sueldo=$sue;
      }
      public function mostrar()
      {
        if($this->sueldo>3000){
          echo $this->nombre;
          echo '<br>';
          echo 'Debe de pagar impuesto';
        }else{
          echo $this->nombre;
          echo '<br>';
          echo 'No debe de pagar impuesto';
        }
      }
    }

    $per1=new Empleado();
    $per1->inicializar('Juan',3500);
    $per1->mostrar();
    echo '<br>';
    $per2=new Empleado();
    $per2->inicializar('Pablo',2000);
    $per2->mostrar();
    ?>
  </body>
</html>