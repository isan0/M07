<html>
  <head>
    <title>PHP Test</title>
  </head>
  <body>
    <?php
    function recibo($precio){
      if($precio<100){
        return 'No se aplica ningun descuento';
      }else if($precio>=100 && $precio<=499.99){
        $descuento=$precio/100*10;
        $total=$precio-$descuento;
        return 'Se aplica un descuento de 10% '.$total;
      }else{
        $descuento=$precio/100*15;
        $total=$precio-$descuento;
        return 'Se aplica un descuento de 15% '.$total;
      }
    }
    echo recibo(550);
    ?>
  </body>
</html>